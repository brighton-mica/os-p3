
#include <stdlib.h>
#include <stdio.h>

struct PriorityNode
{
    size_t studentId;
    struct PriorityNode* next;
};


void print_priorityTable(struct PriorityNode** priorityTable, const size_t levels)
{
    for (size_t i = 0; i < levels; ++i)
    {
        printf("Level %lu: ", i);

        struct PriorityNode* walker = priorityTable[i];
        while (walker != NULL)
        {
            printf("%lu ", walker->studentId);    
            walker = walker->next;
        }
        printf("NULL \n");
    }
}