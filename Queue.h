#include <stdlib.h>
#include <pthread.h>

struct Queue 
{
    size_t head;
    size_t tail;

    size_t reservedSize;
    size_t size;
    int* data;   // student id

    pthread_mutex_t mutex;
};

void queue_init(struct Queue* queue, const size_t size)
{
    queue->head = 0;
    queue->tail = 0;
    queue->size = 0;
    queue->reservedSize = size;
    queue->data = (int*)malloc(sizeof(int) * size);

    pthread_mutex_init(&queue->mutex, NULL);
}

void queue_destroy(struct Queue* queue)
{
    queue->head = 0;
    queue->tail = 0;
    queue->size = 0;
    queue->reservedSize = 0;
    free(queue->data); 

    pthread_mutex_destroy(&queue->mutex);
}

void queue_unsafePush(struct Queue* queue, const int val)
{
    queue->data[queue->tail] = val; 
    queue->tail = (queue->tail + 1) % queue->reservedSize;
    queue->size++;
}

void queue_unsafePop(struct Queue* queue)
{
    if (queue->head == queue->tail)
    {
        pthread_mutex_unlock(&queue->mutex);
        return;
    }

    queue->head = (queue->head + 1) % queue->reservedSize;
    queue->size--;
}

int queue_unsafePeek(struct Queue* queue)
{
    return queue->data[queue->head];
}
