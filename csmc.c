#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>

#include <pthread.h>
#include <semaphore.h>

#include <assert.h>


#include "Queue.h"
#include "PriorityNode.h"

#define CHECK( val ) do { \
    if (val != 0) \
        assert(0); \
} while(0) \

struct Student
{
    sem_t tutorAvailableSemaphore;
    int numHelpsRemaining;
};

struct Tutor
{
    sem_t coordinatorNotifySemaphore;
    int free;
};

pthread_mutex_t gPriorityTableMutex;
struct PriorityNode* gPriorityTable;


//struct Queue gStudentCoordinatorQueue;
int gNumChairs;
int queue[100][2];
int queue2[100];
int prioritytable[100][3];

int start;
int end;
int usedchairs;
pthread_mutex_t queueMutex;
//int* prioritytable


int gNumStudentsAtStart;
struct Student* gStudentArray;

int gNumTutorsAtStart;
struct Tutor* gTutorArray;

pthread_mutex_t gNumChairsMutex;
//int gNumChairs;

sem_t gStudentArrivedSemaphore;

sem_t coordinatorNotifySemaphore;

int gStudentsLeft = 1;

int numHelpsRequired;

int tutorsBusy = 0;

int size=0;

int totalCounts;


void* studentThreadFunc(void* id)
{
    int studentId = (int)id;
    struct Student* student = &gStudentArray[studentId];

    while (student->numHelpsRemaining > 0)
    {
        usleep(rand() % 2000);
        // Add student to end of queue (shared data structure between student and coordinator)
        //printf("trying again %d with level %d",studentId,student->numHelpsRemaining);
        CHECK(pthread_mutex_lock(&queueMutex));
        if(usedchairs>=gNumChairs){
            CHECK(pthread_mutex_unlock(&queueMutex));
            printf("S: Student %lu found no empty chair. Will try again later.\n", studentId);
            continue;
        }
        usedchairs++;
        totalCounts++;
        int y=0;
        for(int i=0;i<gNumChairs;i++){
            if(queue[i][0]==-1){
                y=1;
                queue[i][0]=studentId;
                queue[i][1]=totalCounts;
                break;
            }
        }
        /*if(y==0)
            continue;*/
        printf("trying again %d with level %d",studentId,student->numHelpsRemaining);
        CHECK(sem_post(&gStudentArrivedSemaphore));
        CHECK(pthread_mutex_unlock(&queueMutex));
        
        // wait to be woken up by tutor
        CHECK(sem_wait(&student->tutorAvailableSemaphore));
        printf("heared some response %d with level %d",studentId,student->numHelpsRemaining);
        // get tutored
        usleep(200);
    }
    //printf("I'm going to die %d",studentId);
    pthread_exit(NULL);
}

void* coordinatorThreadFunc()
{
    while (gStudentsLeft)
    {
        // Wait until student says "Hey, I need a tutor!"
        //printf("m waiting to hear");
        CHECK(sem_wait(&gStudentArrivedSemaphore));
        printf("I heard");
        //printf("This is count of executions");
        // Get student id (from shared data structure between student and coordinator)
        CHECK(pthread_mutex_lock(&queueMutex));
        int y=0;
        for(int i=0;i<gNumChairs;i++){
            if(queue[i][0]>-1&&queue2[i]==-1){
                y=1;
                prioritytable[i][0]=queue[i][0];
                prioritytable[i][1]=gStudentArray[queue[i][0]].numHelpsRemaining;
                prioritytable[i][2]=queue[i][1];
                /*printf("C: Student %lu with priority %lu added to the queue. Waiting students now = %d. Total " \
               "requests = %d\n", queue[i], priorityLevel, size, -1);*/
                queue2[i]=queue[i][0];
                printf("coordinator calling thread");
                CHECK(sem_post(&coordinatorNotifySemaphore));
            }
        }
        /*if(y==0)
            printf("ultimate failure");*/
        CHECK(pthread_mutex_unlock(&queueMutex));
    }

    pthread_exit(NULL);
}

void* tutorThreadFunc(void* id)
{
    while (gStudentsLeft){
        int tutorId = (int)id;
        struct Tutor* tutor = &gTutorArray[tutorId];
        CHECK(sem_wait(&coordinatorNotifySemaphore));
        printf("Taking some action");
        //CHECK(pthread_mutex_lock(&gPriorityTableMutex));
        CHECK(pthread_mutex_lock(&queueMutex));
        int pickedprior = -1;
        int curStudentId = -1;
        int pickedno = 2147483647;
        int ind = -1;
        for(int i=0;i<gNumChairs;i++){
            if(prioritytable[i][0]>-1&&prioritytable[i][1]>pickedprior){
                curStudentId=prioritytable[i][0];
                pickedprior=prioritytable[i][1];
                pickedno=prioritytable[i][2];
                ind = i;
            }else if(prioritytable[i][0]>-1&&prioritytable[i][1]==pickedprior&&prioritytable[i][2]<pickedno){
                curStudentId=prioritytable[i][0];
                pickedno=prioritytable[i][2];
                ind=i;
            }
        }
        struct Student* curStudent = &gStudentArray[curStudentId];
        queue[ind][0]=-1;
        queue2[ind]=-1;
        usedchairs--;
        prioritytable[ind][0]=-1;
        curStudent->numHelpsRemaining = curStudent->numHelpsRemaining - 1;
        CHECK(pthread_mutex_unlock(&queueMutex));
        usleep(200);
        tutor->free=1;
        printf("calling the student now %d who is at level%d\n",curStudentId,pickedprior);
        CHECK(sem_post(&curStudent->tutorAvailableSemaphore));
    }
    pthread_exit(NULL);
}


int main(int argc, char* argv[])
{
    assert(argc == 5);
    assert(argv[1] > 0 && argv[2] > 0 && argv[3] > 0 && argv[4] > 0);

    // Init
    gNumStudentsAtStart = (int)atoi(argv[1]);
    gStudentArray = (struct Student*)malloc(sizeof(struct Student) * gNumStudentsAtStart);

    gNumTutorsAtStart = (int)atoi(argv[2]);
    gTutorArray = (struct Tutor*)malloc(sizeof(struct Tutor) * gNumTutorsAtStart);

    gNumChairs = (int)atoi(argv[3]);
    CHECK(pthread_mutex_init(&gNumChairsMutex, NULL));

    // Initialize "gStudentArrivedSemaphore" to 0 because the coordinator thread should
    // block until a student thread posts/signals
    CHECK(sem_init(&gStudentArrivedSemaphore, 0, 0));
    
    // Initialize "coordinatorNotifySemaphore" to 0 because the coordinator thread should
    // block until a student thread posts/signals
    CHECK(sem_init(&coordinatorNotifySemaphore, 0, 0));

    numHelpsRequired = (int)atoi(argv[4]);

    // All students start out with specified number of helps
    // Initialize "tutorAvailableSemaphore" to 0 because a student thread should
    // block until a tutor threads posts/signals
    for (int i = 0; i < gNumStudentsAtStart; ++i)
    {
        gStudentArray[i].numHelpsRemaining = numHelpsRequired;
        CHECK(sem_init(&gStudentArray->tutorAvailableSemaphore, 0, 0));
    }

    // All tutoris are initially available
    for (int i = 0; i < gNumTutorsAtStart; ++i)
    {
        gTutorArray[i].free = 1;
        CHECK(sem_init(&gTutorArray[i].coordinatorNotifySemaphore, 0, 0));
    }

    CHECK(pthread_mutex_init(&gPriorityTableMutex, NULL));
    /*gPriorityTable = (struct PriorityNode*)malloc(sizeof(struct PriorityNode) * numHelpsRequired);
    for (int i = 0; i < numHelpsRequired; ++i)
    {
        gPriorityTable[i].studentId = 2147483647;
        gPriorityTable[i].next = NULL;
        
    }*/
    totalCounts=0;

    //queue_init(&gStudentCoordinatorQueue, (int)gNumChairs);
    //queue = new int[gNumChairs];
    //queue = (int*)malloc(sizeof(int) * gNumChairs);
    for(int i=0;i<gNumChairs;i++){
        queue[i][0] = -1;
        queue2[i] = -1;
        prioritytable[i][0]=-1;
        prioritytable[i][1]=-1;
    }
    /*queue2 = (int*)malloc(sizeof(int) * gNumChairs);
    //queue2 = new int[gNumChairs];
    for(int i=0;i<gNumChairs;i++){
        queue2[i] = -1;
    }*/
    usedchairs=0;
    pthread_mutex_init(&queueMutex, NULL);

    // Thread Creation/Spawn
    pthread_t* studentThreads = (pthread_t*)(malloc(sizeof(pthread_t) * gNumStudentsAtStart));
    for (int i = 0; i < gNumStudentsAtStart; ++i)
    {
        CHECK(pthread_create(&studentThreads[i], NULL, studentThreadFunc, (void *)i));
    }

    pthread_t coordinatorThread;
    CHECK(pthread_create(&coordinatorThread, NULL, coordinatorThreadFunc, NULL));
    
    pthread_t* tutorThreads = (pthread_t*)(malloc(sizeof(pthread_t) * gNumTutorsAtStart));
    for (int i = 0; i < gNumTutorsAtStart; ++i)
    {
        CHECK(pthread_create(&tutorThreads[i], NULL, tutorThreadFunc, (void *)i));
    }


    // Wait until last student has finished
    for (int i = 0; i < gNumStudentsAtStart; ++i)
    {
        pthread_join(studentThreads[i], NULL);
    }

    gStudentsLeft = 0;


    printf("Main Thread Begin Cleanup\n");

    // Cleanup

    //queue_destroy(&gStudentCoordinatorQueue);
    free(gTutorArray);
    free(gStudentArray);

    return 0;
}
